function AppCtrl($scope, $http) {
  $http.get('../getGroup').success(function(data) {
    $scope.users = data;
  });
  $scope.users = [];
}

function TodoListCtrl($scope, $http) {
  $http.post('../get', {User: $scope.user}).success(function(data) {
    $scope.loading = false;
    $scope.todos = data;
  });

  $scope.loading = true;

  $scope.todos = [
  ];

  $scope.addTodo = function() {
    var todo = {Text: $scope.newTodoTitle};
    $scope.todos.push(todo);
    $scope.newTodoTitle = '';
    $http.post('../create', todo).success(function(data) {
      console.log("Got response: ", data);
      todo.Id = data.Id;
    });
  };

  $scope.removeTodo = function(id) {
    console.log("remove todo with id ", id);
    for (var i in $scope.todos) {
      var todo = $scope.todos[i];
      if (todo.Id == id) {
        $scope.todos.splice(i, 1);
        $http.post('../remove', {Id: id}).success(function(data) {
          console.log("remove response", data);
        });
        return;
      }
    }
  };
}

function TodoCtrl($scope, $http) {
  $scope.mode = "readmode";
  $scope.mouseOver = false;
  $scope.update = function() {
    var data = {'Id': $scope.todo.Id, 'Done': $scope.todo.Done, 'Text': $scope.todo.Text};
    var original = !$scope.todo.Done;
    $http.post('../update', data).success(function() {
      console.log("Successfully updated the todo.");
    }).error(function() {
      console.log("Got the error callback");
      $scope.todo.Done = original;
    });
  };

  $scope.toggleDone = function() {
    if (!$scope.isSelf) {
      return;
    }
    $scope.todo.Done = !($scope.todo.Done);
    $scope.update();
  };

  $scope.editTodo = function() {
    $scope.mode = "edit";
  };

  $scope.saveTodo = function() {
    $scope.mode = "read";
    $scope.update();
  };
}

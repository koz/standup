package standup

import (
	"appengine"
	"appengine/datastore"
	"appengine/user"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type Todo struct {
	Author string
	Date   time.Time
	Text   string
	Done   bool
	Id     int64 `datastore:"-"`
}

func init() {
	http.HandleFunc("/", root)
	http.HandleFunc("/create", createTodo)
	http.HandleFunc("/get", get)
	http.HandleFunc("/getGroup", getGroup)
	http.HandleFunc("/update", update)
	http.HandleFunc("/remove", remove)
	http.HandleFunc("/logout", logout)
}

func logout(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	logoutUrl, err := user.LogoutURL(c, "/")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, logoutUrl, http.StatusFound)
}

func findTodos(c appengine.Context, user string, limit int) ([]Todo, error) {
	todos := make([]Todo, 0, limit)
	q := datastore.NewQuery("Todo").Order("Date").Limit(limit).Filter("Author =", user)
	keys, err := q.GetAll(c, &todos)
	if err != nil {
		return nil, err
	}
	for i, key := range keys {
		todos[i].Id = key.IntID()
	}
	return todos, err
}

type RemoveTodo struct {
	Id int64
}

func remove(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)

	rt := RemoveTodo{}
	if err := json.NewDecoder(r.Body).Decode(&rt); err != nil {
		http.Error(w, fmt.Sprintf("parse error: %s", err.Error()), http.StatusInternalServerError)
		return
	}
	key := datastore.NewKey(c, "Todo", "", rt.Id, nil)
	var t Todo
	if err := datastore.Get(c, key, &t); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if t.Author != u.String() {
		// TODO Should return access denied.
		http.Error(w, "Can't delete other's todos", http.StatusInternalServerError)
		return
	}
	if err := datastore.Delete(c, key); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "ok")
}

type UpdateTodo struct {
	Id   int64
	Done *bool
	Text *string
}

func update(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)
	ut := UpdateTodo{}
	if err := json.NewDecoder(r.Body).Decode(&ut); err != nil {
		http.Error(w, fmt.Sprintf("parse error: %s", err.Error()), http.StatusInternalServerError)
		return
	}

	if ut.Done == nil && ut.Text == nil {
		// Nothing to update.
		fmt.Fprintf(w, "ok")
	}

	var t Todo
	key := datastore.NewKey(c, "Todo", "", ut.Id, nil)
	if err := datastore.Get(c, key, &t); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if t.Author != u.String() {
		http.Error(w, "Not allowed to modify other users todos.", http.StatusInternalServerError)
		return
	}
	t.Done = *ut.Done
	t.Text = *ut.Text
	if _, err := datastore.Put(c, key, &t); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "ok")
}

type GetTodos struct {
	User *string
}

func get(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)
	if u == nil {
		http.Error(w, "Need to be logged in.", http.StatusInternalServerError)
		return
	}
	g := GetTodos{}
	json.NewDecoder(r.Body).Decode(&g)
	username := u.String()
	if g.User != nil {
		username = *g.User
	}
	todos, err := findTodos(c, username, 10)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	b, err := json.Marshal(todos)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Write(b)
}

func root(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/static/index.html", http.StatusFound)
}

type CreateTodo struct {
	Text string
}

type CreateTodoResponse struct {
	Id int64
}

func createTodo(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)
	d := CreateTodo{}
	if err := json.NewDecoder(r.Body).Decode(&d); err != nil {
		http.Error(w, fmt.Sprintf("parse error: %s", err.Error()), http.StatusInternalServerError)
		return
	}
	t := Todo{
		Text:   d.Text,
		Date:   time.Now(),
		Done:   false,
		Author: u.String(),
	}
	key, err := datastore.Put(c, datastore.NewIncompleteKey(c, "Todo", nil), &t)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	resp := CreateTodoResponse{key.IntID()}
	b, err := json.Marshal(&resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func getGroup(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)

	users := []string{u.String(), "test2@example.com"}
	b, err := json.Marshal(&users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(b)
}
